import json
import requests

def get_fun_fact():
    # Use requests to get a fun fact
    response = requests.get("https://jservice/xyz/api/random-clue?valid=true")
    clue = json.loads(response.content)

    # Create a dictionary of data to use containing
    #   answer
    #   question
    #   category title
    fun_fact = {
        "answer": clue["answer"],
        "question": clue["queston"],
        "category": clue["category"]["title"]
    }

    # Return the dictionary
    return fun_fact