from django.urls import reverse
from django.db import models

class Blog(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    answer=models.TextField(null=True)
    question=models.TextField(null=True)
    category = models.CharField(max_length=200, null=True)

    def get_api_url(self):
        return reverse("show_blog_detail", kwargs={"pk": self.pk})
        
    def add_comment(self, content):
        comment = Comment.objects.create(
            blog=self,
            content=content,
        )
        self.comments.add(comment)

class Comment(models.Model):
    content = models.TextField()

    blog = models.ForeignKey(
        Blog,
        related_name="comments",
        on_delete=models.CASCADE,
    )
